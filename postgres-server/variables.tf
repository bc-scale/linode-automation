variable "authorized_keys" {
    type = "list"
}
variable "region" {
    default = "us-east"
}
variable "type" {}
variable "types" {
    type = "map"
    default = {
        "Linode 2GB"   = "g6-standard-1"
        "Linode 4GB"   = "g6-standard-2"
        "Linode 8GB"   = "g6-standard-4"
        "Linode 16GB"  = "g6-standard-6"
        "Linode 32GB"  = "g6-standard-8"
        "Linode 64GB"  = "g6-standard-16"
        "Linode 96GB"  = "g6-standard-20"
        "Linode 128GB" = "g6-standard-24"
        "Linode 192GB" = "g6-standard-32"
    }
}
